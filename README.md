# varserver #

**varServer** is a tiny ```node.js``` package that offers a very simple interface to manage *Server Variables*, that are variable accessible for all sessions and throughout requests to the server.

Server variables are stored as a global variable.

## How does it work? ##

```javascript
var varServer = require("varserver")();

// define a variable
varServer.set("appName", "mySweetApp");

// read a variable
console.log( varServer.get("appName") );

// read a variable. If does not exist, create it with a default value
var path = varServer.get("path" , "/")

// delete a variable
varServer.delete("appName");

// test if a variable is defined
console.log( varServer.isDefined("appName") ? "exists":"does not exist");
```