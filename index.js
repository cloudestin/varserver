function setVar( variable , value ) {
	 global.varServerGlobal[variable] = value;
	}
function getVar( variable, defaultValue ) {
	 if (!isDefined( variable ) && (typeof defaultValue !== "undefined")) setVar( variable, defaultValue);
	 return global.varServerGlobal[variable];
	}
function deleteVar( variable ) {
	 delete global.varServerGlobal[variable];
	}
function isDefined( variable ) {
	 return typeof global.varServerGlobal[variable] !== "undefined";
	}

module.exports = function( ){
	 if (!global.varServerGlobal) global.varServerGlobal = {};
	 return {set:setVar, get:getVar, isDefined:isDefined, delete:deleteVar};
	}